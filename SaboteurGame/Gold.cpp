#include "Gold.h"


Gold::Gold()
{
	this->m_gold = GoldEn::None;
}

Gold::Gold(const Gold & other)
{
	m_gold = other.m_gold;
}

Gold::Gold(const GoldEn & gold)
{
	m_gold = gold;
}

const Gold::GoldEn &  Gold::getGold() const
{
	return this->m_gold;
}

void Gold::setGold(const GoldEn & other)
{
	m_gold = other;
}

Gold & Gold::operator=(const Gold & other)
{
	m_gold = other.m_gold;
	return *this;
}

Gold & Gold::operator=(Gold && other)
{
	m_gold = other.m_gold;
	new(&other) Gold;
	return *this;
}

std::ostream & operator<<(std::ostream & os, const Gold & other)
{
	return os <<
		static_cast<int>(other.m_gold);
}
