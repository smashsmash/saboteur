#pragma once
#include "Road.h"
#include "Cardinale.h"
#include<random>
#include <optional>
#include <array>
class Board
{
public:
	using position = std::pair<uint8_t, uint8_t>;
public:
	Board();
	~Board() = default;
	/*Board(const Board& other);
	Board(const Cardinals& type, const Cardinals& firstStonePrize, const Cardinals& secondStonePrize, const Cardinals& goldPrize, const std::vector<Cardinals> cards);*/
public:
	// Getter
	const Cardinals & operator[] (const position& pos) const;
	// Getter and/or Setter
	Cardinals & operator[] (const position& pos);
	//Cardinals operator=(const Cardinals & other);
	friend std::ostream& operator<<(std::ostream& os, const Board& other);

public:
	//Game methods
	bool checkAvailablePositions(const position& poz);
	void removeCard(const position& pos);

private:
	//We know the borders so we create constant variables with 'em
	static const auto height = 5;
	static const auto lenght = 8;
	static const auto dimOfBoard = height * lenght;

private:
	std::array<Cardinals, dimOfBoard> m_cards;

	Cardinals startType;
	Cardinals firstStonePrize;
	Cardinals secondStonePrize;
	Cardinals goldPrize;
};