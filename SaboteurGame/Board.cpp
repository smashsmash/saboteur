#include "Board.h"
#include <array>

Board::Board()
{
	const uint8_t firstLine = 0;
	const uint8_t secondLine = 2;
	const uint8_t thirdLine = 4;
	const uint8_t column = 7;

	const Cardinals weird(Cardinals::North::None, Cardinals::South::None, Cardinals::East::None, Cardinals::West::None);
	for (int i = 0; i < dimOfBoard; ++i)
	{
		m_cards[i].setEast(Cardinals::East::None);
		m_cards[i].setWest(Cardinals::West::None);
		m_cards[i].setSouth(Cardinals::South::None);
		m_cards[i].setNorth(Cardinals::North::None);
	}

	const Cardinals goldPrize(Cardinals::North::Available, Cardinals::South::Available, Cardinals::East::Available, Cardinals::West::Available);
	const Cardinals firstStonePrize(Cardinals::North::Available, Cardinals::South::Blocked, Cardinals::East::Blocked, Cardinals::West::Available);
	const Cardinals secondStonePrize(Cardinals::North::Available, Cardinals::South::Blocked, Cardinals::East::Available, Cardinals::West::Blocked);

	m_cards[secondLine, firstLine] = goldPrize;
	size_t random = rand() % 3;

	if (random == 0)
	{
		m_cards[firstLine, column] = goldPrize;
		m_cards[secondLine, column] = firstStonePrize;
		m_cards[thirdLine, column] = secondStonePrize;
	}
	else {
		if (random == 1)
		{
			m_cards[secondLine,column] = goldPrize;
			m_cards[thirdLine, column] = firstStonePrize;
			m_cards[firstLine, column] = secondStonePrize;

		}
		else
		{
			m_cards[thirdLine, column] = goldPrize;
			m_cards[firstLine, column] = firstStonePrize;
			m_cards[secondLine, column] = secondStonePrize;
		}
	}
	for (auto index1 = 0; index1 < height; ++index1)
	{
		for (auto index = 0; index < lenght; ++index)
		{
			const Cardinals weird(Cardinals::North::None, Cardinals::South::None, Cardinals::East::None, Cardinals::West::None);

			if ((((index1 != 0 && index1 != 2 && index1 != 4) || index != 7) && (index1 != 2 || index != 0)) ){
				m_cards[index1, index] = weird;
			}
		}
	}
}

//Board::Board(const Board & other)
//{
//	for (auto index = 0; index < other.dimOfBoard; index++)
//		m_cards[index] = other.m_cards[index];
//}
//
//Board::Board(const Cardinals & type, const Cardinals & firstSPrize, const Cardinals & secondSPrize, const Cardinals & gPrize, const std::vector<Cardinals> cards)
//{
//	for (auto index = 0; index < this->dimOfBoard; ++index)
//		m_cards[index]=cards[index];
//}

const Cardinals & Board::operator[](const position & pos) const
{
	const auto&[line, column] = pos;

	/*if (line >= height || column >= lenght)
		throw "Board index out of bound.";
	else*/
	return m_cards[line * lenght + column];
}

Cardinals & Board::operator[](const position & pos)
{
	const auto&[line, column] = pos;

	/*if (line >= height || column >= lenght)
		throw "Board index out of bound.";*/

	return m_cards[line * height + column];
}

void Board::removeCard(const position & pos)
{
	//If the position equals the start position, throw exception
	if (pos.first == 0 && pos.second == 2)
		throw "You can't remove the START card!";
	if ((pos.first == 0 || pos.first == 2 || pos.first == 4) && pos.second == 7)
		throw "You can't remove a PRIZE card!";
	if (pos.first >= height || pos.second >= lenght)
		throw "Out of border";

	const Cardinals weird(Cardinals::North::None, Cardinals::South::None, Cardinals::East::None, Cardinals::West::None);

	m_cards[pos.first, pos.second] = weird;
}

bool Board::checkAvailablePositions(const position & poz)
{
	if (m_cards[poz.first, poz.second].m_East != Cardinals::East::None
		&& m_cards[poz.first, poz.second].m_West != Cardinals::West::None
		&& m_cards[poz.first, poz.second].m_North != Cardinals::North::None
		&& m_cards[poz.first, poz.second].m_South != Cardinals::South::None)
	{
		throw "This position is occupied.";
	}


	int left, right;
	
	//columns
	if (poz.second+1 >= 0)
	{
		left = static_cast<int>(m_cards[poz.first, poz.second].m_East);
		right = static_cast<int>(m_cards[poz.first, poz.second + 1].m_West); //already on board
		if (left == right)
		{
			return true;
		}
	}

	if (poz.second - 1 >= 0)
	{
		left = static_cast<int>(m_cards[poz.first, poz.second].m_West);
		right = static_cast<int>(m_cards[poz.first, poz.second - 1].m_East);
		if (left == right)
		{
			return true;
		}
	}
	//lines
	if (poz.first-1 >= 0)
	{
		left = static_cast<int>(m_cards[poz.first, poz.second].m_North);
		right = static_cast<int>(m_cards[poz.first - 1, poz.second].m_South);
		if (left == right)
		{
			return true;
		}
	}

	if (poz.first+1 >= 0)
	{
		left = static_cast<int>(m_cards[poz.first, poz.second].m_South);
		right = static_cast<int>(m_cards[poz.first+1, poz.second].m_North);
		if (left == right)
		{
			return true;
		}
	}
	return false;
}

std::ostream & operator<<(std::ostream & os, const Board & other)
{	const Cardinals wierd(Cardinals::North::None, Cardinals::South::None, Cardinals::East::None, Cardinals::West::None);
	for (auto index1 = 0; index1 < other.height; ++index1)
	{
		for (auto index = 0; index < other.lenght; ++index)
		{
		//if ((index1 == 0 || index1 == 2 || index1 == 4) && index == 7) {
				//os << " PRIZE ";
			//}
		//else {
			if (other.m_cards[index1, index] == wierd)
			{
				os << " _____ ";
			}
			else {
				os << other.m_cards[index1, index] << " ";
			}
		}
		//}
		os << std::endl;
	}
	return os;
}