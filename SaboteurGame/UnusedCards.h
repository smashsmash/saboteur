#pragma once
#include "Cardinale.h"
#include "Road.h"
#include "Actions.h"

#include <array>
class UnusedCards
{
public:
	UnusedCards();
	~UnusedCards() = default;

	friend std::ostream& operator<<(std::ostream& os, const UnusedCards& other);
public:
	const void generateUnusedCards();

public:
	const static size_t kNumOfCards = 71;
	const static size_t kNumOfRoadCards = 44;
	const static size_t kNumOfActionCards = 27;

	std::array<Cardinals, kNumOfRoadCards>m_roadCards;
	std::array<Actions, kNumOfActionCards>m_actionCards;
};

