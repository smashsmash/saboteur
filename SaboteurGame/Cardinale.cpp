#include "Cardinale.h"

Cardinals::Cardinals()
{
	this->m_East = East::None;
	this->m_North = North::None;
	this->m_South = South::None;
	this->m_West = West::None;
}

Cardinals::Cardinals(const Cardinals & other) :
	m_East(other.getEast()),
	m_North(other.getNorth()),
	m_South(other.getSouth()),
	m_West(other.getWest())
{
	//Copy constructor
}

Cardinals::Cardinals(const North & north, const South & south, const East & East, const West & west) :
	m_East(East),
	m_South(south),
	m_West(west),
	m_North(north)
{
	//Copy constructor
}

Cardinals::Cardinals(Cardinals && other)   //for unusedPieces 
{
	*this = std::move(other);
}

const Cardinals::North & Cardinals::getNorth() const
{
	return this->m_North;
}

const Cardinals::South & Cardinals::getSouth() const
{
	return this->m_South;
}

const Cardinals::East & Cardinals::getEast() const
{
	return this->m_East;
}

const Cardinals::West & Cardinals::getWest() const
{
	return this->m_West;
}

const void Cardinals::setNorth(const North & otherN)
{
	this->m_North = otherN;
}

const void Cardinals::setSouth(const South & otherS)
{
	this->m_South = otherS;
}

const void Cardinals::setEast(const East & otherE)
{
	this->m_East = otherE;
}

const void Cardinals::setWest(const West & otherW)
{
	this->m_West = otherW;
}

Cardinals & Cardinals::operator=(const Cardinals & other)
{
	m_East = other.m_East;
	m_West = other.m_West;
	m_South = other.m_South;
	m_North = other.m_North;
	return *this;
}

Cardinals & Cardinals::operator=(Cardinals && other)
{
	m_East = other.m_East;
	m_West = other.m_West;
	m_South = other.m_South;
	m_North = other.m_North;

	new(&other) Cardinals;

	return *this;
}

const bool& Cardinals::operator==(const Cardinals & other)
{
	if (m_East == other.m_East  &&   //un elem de tipul cardinal sa fie egal cu alt elem de tip cardinal
		m_North == other.m_North &&
		m_South == other.m_South &&
		m_West == other.m_West)
		return true;
	return false;
}

const bool& Cardinals::operator==(const Cardinals & other) const
{
	if (m_East == other.m_East  &&   //un elem de tipul cardinal sa fie egal cu alt elem de tip cardinal
		m_North == other.m_North &&
		m_South == other.m_South &&
		m_West == other.m_West)
		return true;
	return false;
}

void Cardinals::rotateCard()
{
	//This two if's verify the stage of the east and west cardinals
	//Because if they have the same value, there is no point to rotate them, we do this just when they're diffrent
	if (m_East == Cardinals::East::Available && m_West == Cardinals::West::Blocked) {
		m_East = Cardinals::East::Blocked;
		m_West = Cardinals::West::Available;
	}
	if (m_East == Cardinals::East::Blocked && m_West == Cardinals::West::Available) {
		m_East = Cardinals::East::Available;
		m_West = Cardinals::West::Blocked;
	}

	//This two if's verify the stage of the north and south cardinals
	//Because if they have the same value, there is no point to rotate them, we do this just when they're diffrent
	if (m_South == Cardinals::South::Available && m_North == Cardinals::North::Blocked) {
		m_South = Cardinals::South::Blocked;
		m_North = Cardinals::North::Available;
	}
	if (m_North == Cardinals::North::Available && m_South == Cardinals::South::Blocked) {
		m_North = Cardinals::North::Blocked;
		m_South = Cardinals::South::Available;
	}
}

std::ostream & operator<<(std::ostream & os, const Cardinals & other)
{
	return os <<
		static_cast<int>(other.m_North) - 2 <<
		static_cast<int>(other.m_East) - 2 <<
		static_cast<int>(other.m_South) - 2 <<
		static_cast<int>(other.m_West) - 2;
}


