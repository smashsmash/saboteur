#include "stdafx.h"
#include "CppUnitTest.h"

#include "..\Cardinale.h"


using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace CardinaleTests
{
	TEST_CLASS(CardinaleTests)
	{
	public:
		TEST_METHOD(DefaultConstructor)
		{
			Cardinals cardinal;
			Assert::IsTrue(cardinal.getNorth() == Cardinals::North::None);
			Assert::IsTrue(cardinal.getSouth() == Cardinals::South::None);
			Assert::IsTrue(cardinal.getEast() == Cardinals::East::None);
			Assert::IsTrue(cardinal.getWest() == Cardinals::West::None);

		}
		TEST_METHOD(CopyContructor)
		{
			Cardinals cardinal(Cardinals::North::Available, Cardinals::South::Blocked, Cardinals::East::DeadEnd, Cardinals::West::Blocked);
			Cardinals piece(cardinal);
			Assert::IsTrue(cardinal == piece);
		}

		TEST_METHOD(ParametersConstructor)
		{
			Cardinals cardinal(Cardinals::North::Available, Cardinals::South::Blocked, Cardinals::East::DeadEnd, Cardinals::West::Blocked);

			Assert::IsTrue(Cardinals::North::Available == cardinal.getNorth());
			Assert::IsTrue(Cardinals::South::Blocked == cardinal.getSouth());
			Assert::IsTrue(Cardinals::East::DeadEnd == cardinal.getEast());
			Assert::IsTrue(Cardinals::West::Blocked == cardinal.getWest());

		}

		TEST_METHOD(Setters)
		{
			Cardinals cardinal;
			cardinal.setNorth(Cardinals::North::Available);
			cardinal.setSouth(Cardinals::South::Blocked);
			cardinal.setEast(Cardinals::East::DeadEnd);
			cardinal.setWest(Cardinals::West::Blocked);
			Assert::IsTrue(cardinal.getNorth() == Cardinals::North::Available);
			Assert::IsTrue(cardinal.getSouth() == Cardinals::South::Blocked);
			Assert::IsTrue(cardinal.getEast() == Cardinals::East::DeadEnd);
			Assert::IsTrue(cardinal.getWest() == Cardinals::West::Blocked);

		}

		TEST_METHOD(RotateCard)
		{
			Cardinals cardinal(Cardinals::North::Available, Cardinals::South::Blocked, Cardinals::East::Blocked, Cardinals::West::Available);
			cardinal.rotateCard();
			Assert::IsTrue(cardinal.getNorth() == Cardinals::North::Blocked);
			Assert::IsTrue(cardinal.getWest() == Cardinals::West::Blocked);
			Assert::IsTrue(cardinal.getSouth() == Cardinals::South::Available);
			Assert::IsTrue(cardinal.getEast() == Cardinals::East::Available);

		}

	};
}