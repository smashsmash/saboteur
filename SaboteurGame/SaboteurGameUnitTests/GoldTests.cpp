#include "stdafx.h"
#include "CppUnitTest.h"

#include"..\Gold.h"


using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace GoldTests
{
	TEST_CLASS(GoldTests)
	{
	public:
		TEST_METHOD(DefaultConstructor)
		{
			Gold gold;
			Assert::IsTrue(gold.getGold() == Gold::GoldEn::None);
		}
		TEST_METHOD(CopyConstructor)
		{
			Gold gold(Gold::GoldEn::Two);
			Gold coin(gold);
			Assert::IsTrue(gold.getGold()==coin.getGold());
		}
		TEST_METHOD(Setters)
		{
			Gold gold;
			gold.setGold(Gold::GoldEn::One);
			Assert::IsTrue(gold.getGold() == Gold::GoldEn::One);
		}
	};
}