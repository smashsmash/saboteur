#include "stdafx.h"
#include "CppUnitTest.h"

#include"..\PlayerType.h"


using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace PlayerTypeTests
{
	TEST_CLASS(PlayerTypeTests)
	{
	public:
		TEST_METHOD(DefaultConstructor)
		{
			PlayerType player;
			Assert::IsTrue(player.getPlayerType() == PlayerType::PlayerTypeEn::None);
		}
		TEST_METHOD(CopyConstructor)
		{
			PlayerType player(PlayerType::PlayerTypeEn::Saboteur);
			PlayerType second(player);
			Assert::IsTrue(player.getPlayerType() == second.getPlayerType());

		}
	};
}