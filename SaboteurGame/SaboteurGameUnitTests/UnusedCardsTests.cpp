#include "stdafx.h"
#include "CppUnitTest.h"

#include"..\Cardinale.h"
#include"..\Actions.h"
#include"..\Road.h"
#include"..\UnusedCards.h"


using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace UnusedCardsTests
{
	TEST_CLASS(UnusedCardsTests)
	{
	public:
		TEST_METHOD(DefaultConstructorActionCard)
		{
			UnusedCards card;
			int verifyIndex = 7;
			Assert::IsTrue(card.m_actionCards[verifyIndex].getSpecialCard() == Actions::specialCards::None);
			Assert::IsTrue(card.m_actionCards[verifyIndex].getToolOne() == Actions::tool::None);
			Assert::IsTrue(card.m_actionCards[verifyIndex].getToolTwo() == Actions::tool::None);
			Assert::IsTrue(card.m_actionCards[verifyIndex].getType() == Actions::type::None);
		}
		TEST_METHOD(DefaultConstructorRoadCard)
		{
			UnusedCards card;
			int verifyIndex = 23;
			Assert::IsTrue(card.m_roadCards[verifyIndex].getNorth() == Cardinals::North::None);
			Assert::IsTrue(card.m_roadCards[verifyIndex].getSouth() == Cardinals::South::None);
			Assert::IsTrue(card.m_roadCards[verifyIndex].getEast() == Cardinals::East::None);
			Assert::IsTrue(card.m_roadCards[verifyIndex].getWest() == Cardinals::West::None);
		}
		TEST_METHOD(GenerateUnusedCardsRoad43)
		{
			//2teste, unul pt un Road card de 43, unul pt 35, 
			//2 la Actions sa fie la 12, 26
			UnusedCards card;
			int verifyIndex = 43;
			card.generateUnusedCards();
			Assert::IsTrue(card.m_roadCards[verifyIndex].getNorth() == Cardinals::North::Blocked);
			Assert::IsTrue(card.m_roadCards[verifyIndex].getSouth() == Cardinals::South::DeadEnd);
			Assert::IsTrue(card.m_roadCards[verifyIndex].getEast() == Cardinals::East::DeadEnd);
			Assert::IsTrue(card.m_roadCards[verifyIndex].getWest() == Cardinals::West::DeadEnd);

		}
		TEST_METHOD(GenerateUnusedCardsRoad35)
		{
			UnusedCards card;
			int verifyIndex = 35;
			card.generateUnusedCards();
			Assert::IsTrue(card.m_roadCards[verifyIndex].getNorth() == Cardinals::North::DeadEnd);
			Assert::IsTrue(card.m_roadCards[verifyIndex].getSouth() == Cardinals::South::DeadEnd);
			Assert::IsTrue(card.m_roadCards[verifyIndex].getEast() == Cardinals::East::DeadEnd);
			Assert::IsTrue(card.m_roadCards[verifyIndex].getWest() == Cardinals::West::DeadEnd);
		}
		TEST_METHOD(GenerateUnusedCardsActions12)
		{
			UnusedCards card;
			int verifyIndex = 12;
			card.generateUnusedCards();
			Assert::IsTrue(card.m_actionCards[verifyIndex].getSpecialCard() == Actions::specialCards::None);
			Assert::IsTrue(card.m_actionCards[verifyIndex].getToolOne() == Actions::tool::Cart);
			Assert::IsTrue(card.m_actionCards[verifyIndex].getToolTwo() == Actions::tool::None);
			Assert::IsTrue(card.m_actionCards[verifyIndex].getType() == Actions::type::Block);
		}
		TEST_METHOD(GenerateUnusedCardsActions26)
		{
			UnusedCards card;
			int verifyIndex = 26;
			card.generateUnusedCards();
			Assert::IsTrue(card.m_actionCards[verifyIndex].getSpecialCard() == Actions::specialCards::None);
			Assert::IsTrue(card.m_actionCards[verifyIndex].getToolOne() == Actions::tool::Pickaxe);
			Assert::IsTrue(card.m_actionCards[verifyIndex].getToolTwo() == Actions::tool::Cart);
			Assert::IsTrue(card.m_actionCards[verifyIndex].getType() == Actions::type::twoUnblock);
		}

	};
}