#include "stdafx.h"
#include "CppUnitTest.h"

#include"..\Player.h"


using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace PlayerTests
{
	TEST_CLASS(PlayerTests)
	{
	public:
		TEST_METHOD(DefaultConstructor)
		{
			Player player;
			Assert::IsTrue(player.name == "");
			/*Assert::IsTrue(player.myCards.size() == Player::numOfPlayerCards);
			Assert::IsTrue(player.rememberRoadCards.size() == Player::numOfPlayerCards);*/
		}
		TEST_METHOD(CopyConstructor)
		{
			Actions blocked(Actions::type::Block, Actions::tool::Cart, Actions::tool::None, Actions::specialCards::None);
			PlayerType myType(PlayerType::PlayerTypeEn::Saboteur);
			Player player("George", blocked, myType);
			Player george(player);
			Assert::IsTrue(player.getName() == george.getName());
			Assert::IsTrue(player.amIBlocked == george.amIBlocked);
			Assert::IsTrue(player.whatAmI.getPlayerType() == george.whatAmI.getPlayerType());

		}
		TEST_METHOD(ParameterConstructor)
		{
			Actions blocked(Actions::type::Block, Actions::tool::Cart, Actions::tool::None, Actions::specialCards::None);
			PlayerType myType(PlayerType::PlayerTypeEn::Saboteur);
			Player player("George", blocked, myType);
			Assert::IsTrue(player.getName() == "George");
			Assert::IsTrue(player.getAmIBlocked().getType() == blocked.getType());
			Assert::IsTrue(player.getWhatAmI().getPlayerType() == myType.getPlayerType());

		}
	};
}