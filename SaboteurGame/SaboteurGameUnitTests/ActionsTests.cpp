#include "stdafx.h"
#include "CppUnitTest.h"

#include"..\Actions.h"


using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace ActionsTests
{
	TEST_CLASS(ActionsTests)
	{
	public:
		TEST_METHOD(DefaultConstructor)
		{
			Actions action;
			Assert::IsTrue(action.getType() == Actions::type::None);
			Assert::IsTrue(action.getToolOne() == Actions::tool::None);
			Assert::IsTrue(action.getToolTwo() == Actions::tool::None);
			Assert::IsTrue(action.getSpecialCard() == Actions::specialCards::None);
		}

		TEST_METHOD(CopyConstructor)
		{
			Actions action(Actions::type::Block, Actions::tool::Pickaxe, Actions::tool::None, Actions::specialCards::None);
			Actions card(action);
			Assert::IsTrue(action == card);

		}

		TEST_METHOD(ParameterConstructor)
		{
			Actions action(Actions::type::Block, Actions::tool::Pickaxe, Actions::tool::None, Actions::specialCards::None);
			Assert::IsTrue(Actions::type::Block == action.getType());
			Assert::IsTrue(Actions::tool::Pickaxe == action.getToolOne());
			Assert::IsTrue(Actions::tool::None == action.getToolTwo());
			Assert::IsTrue(Actions::specialCards::None == action.getSpecialCard());
		}

		TEST_METHOD(Setters)
		{
			Actions action;
			action.setType(Actions::type::Block);
			action.setTools(Actions::tool::Pickaxe, Actions::tool::None);
			action.setSpecialCard(Actions::specialCards::None);
			Assert::IsTrue(action.getType() == Actions::type::Block);
			Assert::IsTrue(action.getToolOne() == Actions::tool::Pickaxe);
			Assert::IsTrue(action.getToolOne() == Actions::tool::Pickaxe);
			Assert::IsTrue(action.getSpecialCard() == Actions::specialCards::None);
		}

		TEST_METHOD(VerifySymbol)
		{
			Actions action(Actions::type::Block, Actions::tool::Pickaxe, Actions::tool::None, Actions::specialCards::None);
			Assert::IsTrue(action.verifySymbol(Actions::type::oneUnblock, { Actions::tool::Pickaxe, Actions::tool::None }));
		}
	};
}