#include "UnusedCards.h"

UnusedCards::UnusedCards()
{
	for (auto id = 0; id < kNumOfActionCards; ++id) {
		m_actionCards[id].setSpecialCard(Actions::specialCards::None);
		m_actionCards[id].setTools(Actions::tool::None, Actions::tool::None);
		m_actionCards[id].setType(Actions::type::None);
	}
	for (auto id = 0; id < kNumOfRoadCards; ++id){
		m_roadCards[id].setEast(Cardinals::East::None);
		m_roadCards[id].setNorth(Cardinals::North::None);
		m_roadCards[id].setSouth(Cardinals::South::None);
		m_roadCards[id].setWest(Cardinals::West::None);
	}
}

const void UnusedCards::generateUnusedCards()
{

	static const size_t numberOfRoadsCard = 35;

	static const size_t numberOfTypeCard = 5;
	//4 types
	static const size_t numberOfDeadEnd = 9;
	//9 types

	//Generate roadCards
	for (auto index = 0; index < numberOfRoadsCard; ++index)
	{
		if (index < numberOfTypeCard) {
			m_roadCards.at(index).m_East = Cardinals::East::Available;
			m_roadCards.at(index).m_North = Cardinals::North::Available;
			m_roadCards.at(index).m_South = Cardinals::South::Available;
			m_roadCards.at(index).m_West = Cardinals::West::Available;
		}
		else
		{
			if (index < numberOfTypeCard * 2) {
				m_roadCards.at(index).m_East = Cardinals::East::Available;
				m_roadCards.at(index).m_West = Cardinals::West::Available;
				m_roadCards.at(index).m_North = Cardinals::North::Blocked;
				m_roadCards.at(index).m_South = Cardinals::South::Blocked;
			}
			else
			{
				if (index < numberOfTypeCard * 3) {
					m_roadCards.at(index).m_East = Cardinals::East::Available;
					m_roadCards.at(index).m_North = Cardinals::North::Available;
					m_roadCards.at(index).m_South = Cardinals::South::Available;
					m_roadCards.at(index).m_West = Cardinals::West::Blocked;
				}
				else
				{
					if (index < numberOfTypeCard * 4) {
						m_roadCards.at(index).m_East = Cardinals::East::Available;
						m_roadCards.at(index).m_North = Cardinals::North::Blocked;
						m_roadCards.at(index).m_South = Cardinals::South::Available;
						m_roadCards.at(index).m_West = Cardinals::West::Available;
					}
					else
					{
						if (index < numberOfTypeCard * 5) {
							m_roadCards.at(index).m_East = Cardinals::East::Blocked;
							m_roadCards.at(index).m_North = Cardinals::North::Available;
							m_roadCards.at(index).m_South = Cardinals::South::Available;
							m_roadCards.at(index).m_West = Cardinals::West::Blocked;
						}
						else
						{
							if (index < numberOfTypeCard * 6) {
								m_roadCards.at(index).m_North = Cardinals::North::Available;
								m_roadCards.at(index).m_South = Cardinals::South::Blocked;
								m_roadCards.at(index).m_West = Cardinals::West::Available;
								m_roadCards.at(index).m_East = Cardinals::East::Blocked;
							}
							else
							{
								m_roadCards.at(index).m_East = Cardinals::East::Available;
								m_roadCards.at(index).m_North = Cardinals::North::Available;
								m_roadCards.at(index).m_South = Cardinals::South::Blocked;
								m_roadCards.at(index).m_West = Cardinals::West::Blocked;
							}
						}
					}
				}
			}
		}
	}
	//Generate deadEndCards

	auto ind = 0;

	m_roadCards[ind + numberOfRoadsCard].m_East = Cardinals::East::DeadEnd;
	m_roadCards[ind + numberOfRoadsCard].m_West = Cardinals::West::DeadEnd;
	m_roadCards[ind + numberOfRoadsCard].m_North = Cardinals::North::DeadEnd;
	m_roadCards[ind + numberOfRoadsCard].m_South = Cardinals::South::DeadEnd;
	ind++;
	m_roadCards[ind + numberOfRoadsCard].m_East = Cardinals::East::DeadEnd;
	m_roadCards[ind + numberOfRoadsCard].m_North = Cardinals::North::Blocked;
	m_roadCards[ind + numberOfRoadsCard].m_South = Cardinals::South::Blocked;
	m_roadCards[ind + numberOfRoadsCard].m_West = Cardinals::West::Blocked;
	ind++;
	m_roadCards[ind + numberOfRoadsCard].m_East = Cardinals::East::DeadEnd;
	m_roadCards[ind + numberOfRoadsCard].m_North = Cardinals::North::Blocked;
	m_roadCards[ind + numberOfRoadsCard].m_South = Cardinals::South::Blocked;
	m_roadCards[ind + numberOfRoadsCard].m_West = Cardinals::West::DeadEnd;
	ind++;
	m_roadCards[ind + numberOfRoadsCard].m_East = Cardinals::East::DeadEnd;
	m_roadCards[ind + numberOfRoadsCard].m_North = Cardinals::North::DeadEnd;
	m_roadCards[ind + numberOfRoadsCard].m_South = Cardinals::South::DeadEnd;
	m_roadCards[ind + numberOfRoadsCard].m_West = Cardinals::West::Blocked;
	ind++;
	m_roadCards[ind + numberOfRoadsCard].m_East = Cardinals::East::Blocked;
	m_roadCards[ind + numberOfRoadsCard].m_North = Cardinals::North::DeadEnd;
	m_roadCards[ind + numberOfRoadsCard].m_South = Cardinals::South::Blocked;
	m_roadCards[ind + numberOfRoadsCard].m_West = Cardinals::West::DeadEnd;
	ind++;
	m_roadCards[ind + numberOfRoadsCard].m_East = Cardinals::East::DeadEnd;
	m_roadCards[ind + numberOfRoadsCard].m_North = Cardinals::North::DeadEnd;
	m_roadCards[ind + numberOfRoadsCard].m_South = Cardinals::South::Blocked;
	m_roadCards[ind + numberOfRoadsCard].m_West = Cardinals::West::Blocked;
	ind++;
	m_roadCards[ind + numberOfRoadsCard].m_East = Cardinals::East::Blocked;
	m_roadCards[ind + numberOfRoadsCard].m_North = Cardinals::North::DeadEnd;
	m_roadCards[ind + numberOfRoadsCard].m_South = Cardinals::South::Blocked;
	m_roadCards[ind + numberOfRoadsCard].m_West = Cardinals::West::Blocked;
	ind++;
	m_roadCards[ind + numberOfRoadsCard].m_East = Cardinals::East::Blocked;
	m_roadCards[ind + numberOfRoadsCard].m_North = Cardinals::North::DeadEnd;
	m_roadCards[ind + numberOfRoadsCard].m_South = Cardinals::South::DeadEnd;
	m_roadCards[ind + numberOfRoadsCard].m_West = Cardinals::West::Blocked;
	ind++;
	m_roadCards[ind + numberOfRoadsCard].m_East = Cardinals::East::DeadEnd;
	m_roadCards[ind + numberOfRoadsCard].m_North = Cardinals::North::Blocked;
	m_roadCards[ind + numberOfRoadsCard].m_South = Cardinals::South::DeadEnd;
	m_roadCards[ind + numberOfRoadsCard].m_West = Cardinals::West::DeadEnd;

	//Generate actionCards
	///if last bit != -1 it means that is a blocking/unblocking card

	// 6 maps, 3 stonefalls => -1-1-10 - stoneFall; -1-1-11 - Map

	// 3 of each tool -> block => 
	// 3 * Pickaxe block (00-1-1) ; 
	// 3 * Cart block (01-1-1);
	// 3 * Lantern block (02-1-1);

	// 2 of each tool -> oneUnblock
	// 2 * Pickaxe unblock (10-1-1) ;
	// 2 * Cart unblock (11-1-1);
	// 2 * Lantern unblock (12-1-1);

	// 1 of each combinationTool -> twoUnblock
	// 1 * Pickaxe + Cart unblock (101-1);
	// 1 * Pickaxe + Lantern unblock (102-1);
	// 1 * Cart + Lantern unblock (112-1);

	static const size_t numbOfBlockAndStoneAndTools = 3;
	static const size_t numbOfOneUnblock = 2;
	static const size_t numbOfTwoUnblock = 1;

	static const size_t numbOfMaps = numbOfBlockAndStoneAndTools * numbOfOneUnblock;
	static size_t index;

	//adding 6 map cards
	for (index = 0;
		index < numbOfMaps;
		++index) {
		m_actionCards[index].setSpecialCard(Actions::specialCards::Map);
		m_actionCards[index].setTools(Actions::tool::None, Actions::tool::None);
		m_actionCards[index].setType(Actions::type::None);
	}

	//adding 3 stone fall cards => Tot: 9
	for (index = numbOfMaps;
		index < numbOfMaps + numbOfBlockAndStoneAndTools;
		++index) {
		m_actionCards[index].setSpecialCard(Actions::specialCards::StoneFall);
		m_actionCards[index].setTools(Actions::tool::None, Actions::tool::None);
		m_actionCards[index].setType(Actions::type::None);
	}

	//adding 3 pickaxe block cards => Tot: 12
	for (index = numbOfMaps + numbOfBlockAndStoneAndTools;
		index < numbOfMaps + numbOfBlockAndStoneAndTools * 2;
		++index) {
		m_actionCards[index].setSpecialCard(Actions::specialCards::None);
		m_actionCards[index].setTools(Actions::tool::Pickaxe, Actions::tool::None);
		m_actionCards[index].setType(Actions::type::Block);
	}

	//adding 3 cart block cards => Tot: 15
	for (index = numbOfMaps + numbOfBlockAndStoneAndTools * 2;
		index < numbOfMaps + numbOfBlockAndStoneAndTools * 3;
		++index) {
		m_actionCards[index].setSpecialCard(Actions::specialCards::None);
		m_actionCards[index].setTools(Actions::tool::Cart, Actions::tool::None);
		m_actionCards[index].setType(Actions::type::Block);
	}

	//adding 3 lantern block cards => Tot: 18
	for (index = numbOfMaps + numbOfBlockAndStoneAndTools * 3;
		index < numbOfMaps * 3;
		++index) {
		m_actionCards[index].setSpecialCard(Actions::specialCards::None);
		m_actionCards[index].setTools(Actions::tool::Lantern, Actions::tool::None);
		m_actionCards[index].setType(Actions::type::Block);
	}

	//adding 2 pickaxe one unblock => Tot: 20
	for (index = numbOfMaps * 3;
		index < numbOfMaps * 3 + numbOfOneUnblock;
		++index) {
		m_actionCards[index].setSpecialCard(Actions::specialCards::None);
		m_actionCards[index].setTools(Actions::tool::Pickaxe, Actions::tool::None);
		m_actionCards[index].setType(Actions::type::oneUnblock);
	}

	//adding 2 cart one unblock => Tot: 22
	for (index = numbOfMaps * 3 + numbOfOneUnblock;
		index < numbOfMaps * 3 + numbOfOneUnblock * 2;
		++index) {
		m_actionCards[index].setSpecialCard(Actions::specialCards::None);
		m_actionCards[index].setTools(Actions::tool::Cart, Actions::tool::None);
		m_actionCards[index].setType(Actions::type::oneUnblock);
	}

	//adding 2 lantern one unblock => Tot:24
	for (index = numbOfMaps * 3 + numbOfOneUnblock * 2;
		index < numbOfMaps * 3 + numbOfOneUnblock * 3;
		++index) {
		m_actionCards[index].setSpecialCard(Actions::specialCards::None);
		m_actionCards[index].setTools(Actions::tool::Lantern, Actions::tool::None);
		m_actionCards[index].setType(Actions::type::oneUnblock);
	}

	//adding cart + lantern two unblock => Tot: 25
	for (index = numbOfMaps * 3 + numbOfOneUnblock * 3;
		index < numbOfMaps * 3 + numbOfOneUnblock * 3 + numbOfTwoUnblock;
		++index) {
		m_actionCards[index].setSpecialCard(Actions::specialCards::None);
		m_actionCards[index].setTools(Actions::tool::Cart, Actions::tool::Lantern);
		m_actionCards[index].setType(Actions::type::twoUnblock);
	}

	//adding Pickaxe + lantern two unblock => Tot: 26
	for (index = numbOfMaps * 3 + numbOfOneUnblock * 3 + numbOfTwoUnblock;
		index < numbOfMaps * 3 + numbOfOneUnblock * 3 + numbOfTwoUnblock * 2;
		++index) {
		m_actionCards[index].setSpecialCard(Actions::specialCards::None);
		m_actionCards[index].setTools(Actions::tool::Pickaxe, Actions::tool::Lantern);
		m_actionCards[index].setType(Actions::type::twoUnblock);
	}

	//adding Pickaxe + cart two unblock => Tot: 27
	for (index = numbOfMaps * 3 + numbOfOneUnblock + numbOfTwoUnblock * 2;
		index < numbOfMaps * 3 + numbOfOneUnblock * 3 + numbOfTwoUnblock * 3;
		++index) {
		m_actionCards[index].setSpecialCard(Actions::specialCards::None);
		m_actionCards[index].setTools(Actions::tool::Pickaxe, Actions::tool::Cart);
		m_actionCards[index].setType(Actions::type::twoUnblock);
	}
}

std::ostream& operator<<(std::ostream & os, const UnusedCards & other)
{
	for (auto i = 0; i < other.kNumOfRoadCards; ++i)
		os << other.m_roadCards[i] << " ";
	os << std::endl;
	for (auto i = 0; i < other.kNumOfActionCards; ++i)
		os << other.m_actionCards[i] << " ";
	return os;
}
