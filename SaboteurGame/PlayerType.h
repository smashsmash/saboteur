#pragma once
#include <iostream>
class PlayerType
{
public:

	enum class PlayerTypeEn :uint8_t
	{
		None,
		Saboteur,
		Midget
	};

public:
	PlayerType();
	PlayerType(const PlayerTypeEn& other);
	PlayerType(const PlayerType& other);
	~PlayerType() = default;

	const PlayerTypeEn& getPlayerType() const;

	PlayerType& operator = (const PlayerType& other);
	PlayerType& operator = (PlayerType&& other);

	friend std::ostream& operator<<(std::ostream& os, const PlayerType& other);

public:
	PlayerTypeEn m_playerType : 2;
};
