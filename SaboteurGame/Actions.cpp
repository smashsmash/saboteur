#include "Actions.h"



Actions::Actions()
{
	m_type = type::None;
	m_toolOne = tool::None;
	m_toolTwo = tool::None;
	m_card = specialCards::None;
}

Actions::Actions(const Actions & other):
	m_type(other.getType()),
	m_card(other.getSpecialCard()),
	m_toolOne(other.getToolOne()),
	m_toolTwo(other.getToolTwo())

{
	//Copy constructor
}


Actions::Actions(const type & type, const tool & toolOne, const tool & toolTwo, const specialCards card):
	m_type(type),
	m_card(card),
	m_toolOne(toolOne),
	m_toolTwo(toolTwo)
{
	//Copy constructor
}
Actions::~Actions()
{
	m_type = type::None;
	m_toolOne = tool::None;
	m_toolTwo = tool::None;
	m_card = specialCards::None;
}

Actions & Actions::operator=(const Actions & other)
{
	m_card = other.m_card;
	m_toolOne = other.m_toolOne;
	m_toolTwo = other.m_toolTwo;
	m_type = other.m_type;

	return *this;
}

Actions & Actions::operator=(Actions && other)
{
	m_card = other.m_card;
	m_toolOne = other.m_toolOne;
	m_toolTwo = other.m_toolTwo;
	m_type = other.m_type;

	new(&other) Actions;

	return *this;
}

bool Actions::operator==(const Actions & other)
{
	if (m_card == other.m_card && m_toolOne == other.m_toolOne && m_toolTwo == other.m_toolTwo && m_type == other.m_type)
	{
		return true;
	}
	return false;
}

bool Actions::operator!=(const Actions & other)
{
	if (m_card != other.m_card || m_toolOne != other.m_toolOne || m_toolTwo != other.m_toolTwo || m_type != other.m_type)
	{
		return true;
	}
	return false;
}


const bool Actions::verifySymbol(const type & bCard, const tools & boCard) const
{
	//tipul cartii mele(blocked) sa fie diferit de cartea din param(unblock)
	if (static_cast<int>(this->m_type) != static_cast<int>(bCard) &&
		(static_cast<int>(boCard.first) == static_cast<int>(this->m_toolOne) ||
			static_cast<int>(boCard.second) == static_cast<int>(this->m_toolTwo)))
		return true;
	return false;
}

const Actions::type & Actions::getType() const
{
	return m_type;
}

const Actions::tool & Actions::getToolOne() const
{
	return m_toolOne;
}

const Actions::tool & Actions::getToolTwo() const
{
	return m_toolTwo;
}

const Actions::specialCards & Actions::getSpecialCard() const
{
	return m_card;
}

void Actions::setType(const type & otherType)
{
	this->m_type = otherType;
}

void Actions::setTools(const tool & otherToolOne, const tool & otherToolTwo)
{
	m_toolOne = otherToolOne;
	m_toolTwo = otherToolTwo;
}

void Actions::setSpecialCard(const specialCards & otherSpecialCards)
{
	m_card = otherSpecialCards;
}

std::ostream & operator<<(std::ostream & out, const Actions & other)
{
	return out << static_cast<int>(other.m_type) - 1
		<< static_cast<int>(other.m_toolOne) - 1
		<< static_cast<int>(other.m_toolTwo) - 1
		<< static_cast<int>(other.m_card) - 1;
}


