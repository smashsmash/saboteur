#include "Player.h"



Player::Player()
{
	name = "";
	myCards.reserve(numOfPlayerCards);
	rememberRoadCards.reserve(numOfPlayerCards);
}

Player::Player(const Player & other)
{
	name = other.name;
	amIBlocked = other.amIBlocked;
	whatAmI = other.whatAmI;
	for (auto i = 0; i < numOfPlayerCards; ++i)
		myCards[i] = other.myCards[i];
}

Player::Player(const std::string & name, Actions & blocked, PlayerType & amIi)
{
	setName(name);
	amIBlocked = blocked;
	whatAmI = amIi;
}

//We'll move the piece given as an argument in burned cards.
const void Player::BlockPlayer(Player& other, const Actions & blockedCard)
{
	//The player should not be blocked already
	if (other.amIBlocked.getType() == Actions::type::Block)
	{
		throw "The player is already blocked. ";
	}
	other.amIBlocked = blockedCard;
}

const void Player::UnblockPlayer(Player& other, const Actions & unblockCard)
{
	//To block a player we must verifySimbol
	if (other.amIBlocked.getType() != Actions::type::Block)
	{
		throw "You can't unblock a player that is not blocked. ";
	}

	//We unblocked the player
	Actions::tools unblockedCardTools;
	unblockedCardTools.first = unblockCard.getToolOne();
	unblockedCardTools.second = unblockCard.getToolTwo();
	if (other.amIBlocked.verifySymbol(unblockCard.getType(), unblockedCardTools) == true)
	{
		other.amIBlocked = unblockCard;
	}

	//and burn cards
}

void Player::PlaceCard(Cardinals & piece, Board& board)
{
	Board::position pos;
	/*uint16_t line = UINT16_MAX;
	uint16_t column = UINT16_MAX;

	if (in >> line)
		if (in >> column)
		{
			Board::position position = { static_cast<uint8_t>(line), static_cast<uint8_t>(column) };

			auto& optionalPiece = board[position];
			const Cardinals wierd(Cardinals::North::None, Cardinals::South::None, Cardinals::East::None, Cardinals::West::None);

			if (optionalPiece == wierd) {
				optionalPiece = std::move(piece);

				return position;
			}
			else
				throw "That position is occupied by another piece.";
			
		}

	in.clear();
	in.seekg(std::ios::end);
	throw "Please enter numbers from 1 to 5 for lines, and from 1 to 8 for columns.";*/
	std::cout << "Enter line(1-5)\n";
	std::cin >> pos.first;
	--pos.first;
	std::cout << "Enter column(1-8)\n";
	std::cin >> pos.second;
	--pos.second;

	if ((pos.first < 0 || pos.first>4) && (pos.second < 0 && pos.second>7))
		throw "Out of index!!\n";

	board[pos] = std::move(piece);
}

const void Player::TakeCard(const size_t& index, UnusedCards& ue, std::string type)
{

	size_t random1 = rand() % ue.m_actionCards.size();
	size_t random2 = rand() % ue.m_roadCards.size();
	size_t randomTotal = rand() % 2;

	if (type == "a") {
		for (auto i = index; i < myCards.size() - 1; ++i) {
			myCards[i] = myCards[i + 1];
		}
	}
	else {
		for (auto i = index; i < rememberRoadCards.size() - 1; ++i) {
			rememberRoadCards[i] = rememberRoadCards[i + 1];
		}
	}

	myCards.shrink_to_fit();
	rememberRoadCards.shrink_to_fit();

	if (randomTotal == 0)
	{
		auto i = myCards.size() + 1;
		myCards.resize(i);
		myCards[i] = std::move(ue.m_actionCards[random1]);
		myCards.shrink_to_fit();
	}
	else
	{
		auto i = rememberRoadCards.size() + 1;
		rememberRoadCards.resize(i);
		rememberRoadCards[i] = std::move(ue.m_roadCards[random2]);
		rememberRoadCards.shrink_to_fit();
	}
}

const void Player::GeneratePlayerCards(UnusedCards& ue)
{
	auto index = 0;
	auto indexR = 0;
	auto indexA = 0;

	rememberRoadCards.reserve(numOfPlayerCards);
	myCards.reserve(numOfPlayerCards);

	while (index < numOfPlayerCards)
	{
		size_t random1 = rand() % (ue.m_actionCards.size());
		size_t random2 = rand() % (ue.m_roadCards.size());
		size_t randomTotal = rand() % 2;

		if (randomTotal == 0)
		{
			myCards.resize(indexA + 1);
			myCards[indexA] = std::move(ue.m_actionCards[random1]);
			++indexA;
			myCards.shrink_to_fit();
		}
		else
		{
			rememberRoadCards.resize(indexR + 1);
			rememberRoadCards[indexR] = std::move(ue.m_roadCards[random2]);
			++indexR;
			rememberRoadCards.shrink_to_fit();
		}
		++index;
	}
	--indexA;
	myCards.shrink_to_fit();
	--indexR;
	rememberRoadCards.shrink_to_fit();
}

const std::string & Player::getName() const
{
	return name;
}

const Actions & Player::getAmIBlocked() const
{
	return amIBlocked;
}

const PlayerType & Player::getWhatAmI() const
{
	return whatAmI;
}

void Player::setName(const std::string & other)
{
	name = other;
}

void Player::setPlayerType(const PlayerType & other)
{
	whatAmI = other;
}

const bool & Player::operator==(const Player & other) const
{
	if (other.getName() == this->getName() &&
		this->getAmIBlocked().getType() == other.getAmIBlocked().getType() &&
		this->getWhatAmI().getPlayerType() == other.getWhatAmI().getPlayerType()) {
		return true;
	}
	return false;
}
