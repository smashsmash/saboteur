#pragma once
#include <iostream>

/*
I realized that if we want to interpratate the cards with cardinals we will need this class
We will use this in the class Road as it is described there
*/
class Cardinals
{
public:
	enum class North : uint8_t {
		None,     // -2
		DeadEnd,  // -1
		Blocked,  //  0
		Available //  1 
	};
	enum class South : uint8_t {
		None,
		DeadEnd,
		Blocked,
		Available,
	};
	enum class East : uint8_t {
		None,
		DeadEnd,
		Blocked,
		Available
	};
	enum class West : uint8_t {
		None,
		DeadEnd,
		Blocked,
		Available
	};
public:
	//	Constructors
	Cardinals();
	Cardinals(const Cardinals& other);
	Cardinals(const North& north, const South& south, const East& East, const West& west);
	Cardinals(Cardinals&& other);

	//	Destructor
	~Cardinals() = default;

	// Getters
	const North& getNorth() const;
	const South& getSouth() const;
	const East& getEast() const;
	const West& getWest() const;
	
	//setters
	const void setNorth(const North& otherN);
	const void setSouth(const South& otherS);
	const void setEast(const East& otherE);
	const void setWest(const West& otherW);

	//Overwrited operators
	Cardinals& operator = (const Cardinals& other);
	Cardinals& operator = (Cardinals&& other);
	const bool& operator==(const Cardinals& other);

	const bool & operator==(const Cardinals& other) const;

	//Output overwritten operator
	friend std::ostream& operator<<(std::ostream& os, const Cardinals& other);

	//A function that will help us if the player wants to rotate a card
	void rotateCard();

public:
	North m_North : 2;   // :2 -> max 4 componenents
	South m_South : 2;
	East m_East : 2;
	West m_West : 2;
};