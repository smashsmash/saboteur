#include "PlayerType.h"



PlayerType::PlayerType()
{
	m_playerType = PlayerTypeEn::None;
}

PlayerType::PlayerType(const PlayerTypeEn& other)
{
	m_playerType = other;
}

PlayerType::PlayerType(const PlayerType & other)
{
	this->m_playerType = other.getPlayerType();
}

const PlayerType::PlayerTypeEn & PlayerType::getPlayerType() const
{
	return this->m_playerType;
}

PlayerType & PlayerType::operator=(const PlayerType & other)
{
	m_playerType = other.m_playerType;
	return *this;
}

PlayerType & PlayerType::operator=(PlayerType && other)
{
	m_playerType = other.m_playerType;
	new(&other) PlayerType;
	return *this;
}

std::ostream & operator<<(std::ostream & os, const PlayerType & other)
{
	return os << static_cast<int>(other.m_playerType) - 1 << " ";
}