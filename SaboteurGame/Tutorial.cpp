#include "Tutorial.h"
#include <iostream>

void Tutorial::saboteurTutorial()
{
	std::cout << "						GAME TUTORIAL\n";
	std::cout << "			The game is composed by gold miners and saboteurs.\n\n The number of players influence de number of gold miners and saboteurs this way:\n";
	std::cout << "	 3 players: 1 saboteur  and 3 gold miners\n";
	std::cout << "	 4 players: 1 saboteur  and 4 gold miners\n";
	std::cout << "	 5 players: 2 saboteurs and 4 gold miners\n";
	std::cout << "	 6 players: 2 saboteurs and 5 gold miners\n";
	std::cout << "	 7 players: 3 saboteurs and 5 gold miners\n";
	std::cout << "	 8 players: 3 saboteurs and 6 gold miners\n";
	std::cout << "	 9 players: 3 saboteurs and 7 gold miners\n";
	std::cout << "	10 players: 4 saboteurs and 7 gold miners\n\n";
	std::cout << "We will shuffle the required numbers of gold miners and saboteurs together.\n";
	std::cout << "Each player will recieve one card, each of you will look at his own ";
	std::cout << "without revealing his role to the other players.\n\n";
	std::cout << "You will have displayed the start card and three goal cards ";
	std::cout << "(one with the gold treasure and two with mere stones) from among the 44 path cards.\n";
	std::cout << "The goal cards will be shuffled.\n\n";
	std::cout << " Over the course of the game, a maze of pathways from the start card to the goal cards is created. \n";
	std::cout << " The remaining path cards and all action cards are shuffled and dealt out a number of cards to each player, which also depends on the number of players in the game : \n\n";
	std::cout << "3 to 5 players: Each player is dealt 6 cards. \n";
	std::cout << "6 to 7 players: Each player is dealt 5 cards\n";
	std::cout << "8 to 10 players : Each player is dealt 4 cards \n";
	std::cout << "On his turn, a player must first play a card.\n";
	std::cout << "This means: - Either add a path card to the maze,\n";
	std::cout << "- or put down an action card in front of a player,\n";
	std::cout << "- or pass, putting one card face down on the discard pile.\n";
	std::cout << "After that, the player gets a card from the deck into his hand. This ends his turn, and play passes to\n";
	std::cout << "the next player.\n";
	std::cout << "Note: As soon as the deck is used up, players do not get cards anymore; a turn then consists of playing\n";
	std::cout << "a card or passing (with a discard).\n";
	std::cout << "Playing a path card:\n";
	std::cout << "Bit by bit, the path cards form a way from the start\n";
	std::cout << "card to the goal cards. A path card must always be\n";
	std::cout << "put next to a path card that is already on the table.\n";
	std::cout << "The paths on all sides of the card must fit those\n";
	std::cout << "already in play, and a path card may never be played\n";
	std::cout << "crosswise(vertically). The gold-diggers will try to establish an\n";
	std::cout << "uninterrupted path from the start card to one of the goal cards; the\n";
	std::cout << "saboteurs will attempt to prevent just that. However, they should not\n";
	std::cout << "do so too obviously, otherwise they will be unmasked very quickly.\n";
	std::cout << "Playing an action card:\n";
	std::cout << "Action cards are played by playing them on yourself or another player. By means of action\n";
	std::cout << "cards, players can hinder or help each other, take a card out of the pathway maze, or gain information\n";
	std::cout << " about the goal cards.\n";
	std::cout << "If one of the Block cards is put on a player, that player may\n";
	std::cout << "not play a path card as long as this card is in place. Of course, he may\n";
	std::cout << "still play other cards. \n";
	std::cout << "Unblock cards are used to repair the broken tools(Block cards) to remove one\n";
	std::cout << "of the Block cards from in front of a player. They may be played\n";
	std::cout << "on a card in front of yourself, or on one in front of a fellow player.\n";
	std::cout << "Either way, both the repair(Unblock) card and the broken tool(Block) card are put on\n";
	std::cout << "the discard pile after use. Of course, the repair card must match the broken tool\n";
	std::cout << "� for example, if there is a broken cart in front of a player, it\n";
	std::cout << "can only be repaired by an intact cart, not by a lantern or pickaxe.\n";
	std::cout << "There are also repair cards showing two tools. If one of these is played,\n";
	std::cout << "it can be used to repair either one of the tools shown, but not\n";
	std::cout << "both.\n";
	std::cout << "Note: All repair cards can only be played if there is an appropriate\n";
	std::cout << "broken tool in front of a player.\n";
	std::cout << "This card, the stone-fall, a player puts down in front of himself. He may then take a path card of\n";
	std::cout << "his choice (except start and goal cards) out of the maze of pathways, putting it and the rock\n";
	std::cout << "fall card in the discard pile. A saboteur can thus interrupt a path from the start card\n";
	std::cout << "towards a goal card; a gold-digger can take out a dead end, providing a new chance for a path.\n";
	std::cout << "Gaps created in this way can be filled with fitting path cards in the following turns.\n";
	std::cout << "When a player plays the Map card, he carefully picks up one of the three goal cards, looks at it,\n";
	std::cout << "then the goal card is put back in its place and this card in the discard pile. He now knows\n";
	std::cout << "whether or not it is worth digging a path to this particular goal card � for only one of the\n";
	std::cout << "three shows the treasure.\n";
	std::cout << "Pass\n";
	std::cout << "If a player cannot or does not want to play a card, he must pass, putting one\n";
	std::cout << "card from his hand in the discard pile. Towards the end\n";
	std::cout << "of a round, there is a chance that players may have no cards left in hand � in\n";
	std::cout << "which case they also pass (without discarding, of course).\n";
	std::cout << "End of a round\n";
	std::cout << "When a player plays a path card so that it reaches a goal card and creates an\n";
	std::cout << "uninterrupted path (of at least 7 cards) from the start card to this goal card, he\n";
	std::cout << "turns it over.\n";
	std::cout << "� If it is the card showing the treasure, the round is over.\n";
	std::cout << "� If it shows a stone, the round continues. The face-up goal card is placed\n";
	std::cout << "next to the path card just played in such a way that the paths fit.\n";
	std::cout << "Note: In rare cases, it may be that the goal card cannot be placed in such a\n";
	std::cout << "way that all paths fit the adjacent path cards. As an exception to the general\n";
	std::cout << "rule, this is allowed if it concerns a goal card.\n";
	std::cout << "The round is also over if the deck is used up and each player in turn has passed\n";
	std::cout << "because he has no playable cards left in hand. Now, all dwarf cards are turned\n";
	std::cout << "over: Who was a gold-digger, and who a saboteur?\n";
	std::cout << "Handing out the gold\n";
	std::cout << "The gold-diggers have won the round if an uninterrupted path has been formed\n";
	std::cout << "from the start card to the goal card showing the treasure. Draw as many gold nugget cards (face down)\n";
	std::cout << "as there are players � e.g., five cards if there are five players. (However, in a 10-player game, draw only\n";
	std::cout << "nine nugget cards).\n";
	std::cout << "The player who reached the goal card with the treasure gets these gold nugget cards and chooses one\n";
	std::cout << "card. Then the remaining gold nugget cards is passed counter-clockwise to the next gold-digger (not\n";
	std::cout << "saboteur), who also chooses a card. This goes on until all gold nugget cards have been chosen. It may\n";
	std::cout << "well be that some gold-diggers get more cards than others.\n";
	std::cout << "The saboteurs have won the round if the goal card showing the treasure cannot be reached or the deck\n";
	std::cout << "has been depleted and everyone has passed. If there was only one saboteur, he gets gold nugget cards\n";
	std::cout << "worth a total of four nuggets (not four gold nugget cards). If there were two or three saboteurs, each\n";
	std::cout << "gets three nuggets worth of gold; if there were four saboteurs, each gets two nuggets. If there are no\n";
	std::cout << "saboteurs, which can happen in a three or four player game, no gold nuggets are handed out.\n";
	std::cout << "The players keep their gold cards secret until the end of the game.\n";
	std::cout << "A new round begins\n";
	std::cout << "Once the gold has been handed out, the next round begins. Start card and goal cards are on the table\n";
	std::cout << "just like at the beginning of the game. The same dwarf cards used for the last round (including the one that\n";
	std::cout << "was put aside) are shuffled and dealt out again. The path and action cards are shuffled, dealt out the appropriate number\n";
	std::cout << "of cards to the players, and put the rest down as a deck.\n";
	std::cout << "Of course, players keep the gold nugget cards they have earned in previous rounds.\n";
	std::cout << "End of the game\n";
	std::cout << "The game ends after the third round. We will count the gold nuggets on all the players gold nugget cards.\n";
	std::cout << "The player with the most nuggets wins the game. If there is more than one player with the same number\n";
	std::cout << "of nuggets, they are tied in first place.\n";
	system("pause");
	system("cls");
}
