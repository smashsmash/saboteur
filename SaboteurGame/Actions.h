#pragma once
#include <array>

class Actions 
{
	//This enum contains all block-type cards, 3 each
public:
	enum class type : uint8_t
	{
		None,			//0 (-1)
		Block,			//1 ( 0)
		oneUnblock,		//2 ( 1)
		twoUnblock,		//3 ( 2)
	};

	enum class tool : uint8_t
	{
		None,			//0 (-1)
		Pickaxe,		//1 ( 0)
		Cart,			//2 ( 1)
		Lantern,		//3 ( 2)

	};

	//This contains all special cards, 6 maps, 3 stonefalls
	enum class specialCards : uint8_t
	{
		None,			//0 (-1)
		Map,			//1 ( 0)
		StoneFall		//2 ( 1)
	};

public:
	using tools = std::pair<tool, tool>;
public:

	//Constructors
	Actions();
	Actions(const Actions& other);
	Actions(const type & type, const tool & toolOne, const tool & toolTwo, const specialCards card);
	
	//Destructor
	~Actions();

	//Overwrited operators
	bool operator==(const Actions &other);
	bool operator!=(const Actions &other);
	Actions& operator = (const Actions& other);
	Actions& operator = (Actions&& other);

	//Output overwritten operator
	friend std::ostream& operator << (std::ostream& out, const Actions &other);
	
	//Checks if the dead end can be unblocked with the wanted card.
	const bool verifySymbol(const type& bCard, const tools& boCard)const;

	//getters
	const type& getType() const;
	const Actions::tool & getToolOne() const;
	const Actions::tool & getToolTwo() const;
	const specialCards& getSpecialCard() const;

	//setters
	void setType(const type& otherType);
	void setTools(const tool & otherToolOne, const tool & otherToolTwo);
	void setSpecialCard(const specialCards& otherSpecialCards);

public:
	static const size_t numberOfActions = 27;

	type m_type : 2;
	tool m_toolOne : 2;
	tool m_toolTwo : 2;
	specialCards m_card : 2;
};

