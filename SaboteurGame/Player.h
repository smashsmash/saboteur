#pragma once

#include "Board.h"
#include "UnusedCards.h"
#include "Actions.h"
#include "PlayerType.h"
#include <array>
class Player
{
public:
	//constructors
	Player();
	Player(const Player& other);
	Player(const std::string& name, Actions& blocked, PlayerType& amI);
	//destructor
	~Player() = default;

public:
	//The player has the possibility of passing his turn, and then the player has to replace a card
	const void Pass(const size_t& index, UnusedCards& ue);
	//The player has the possibility of blocking another player
	const void BlockPlayer(Player& other, const Actions & blockedCard);
	//The player has the possibility of unblocking another player or himself
	const void UnblockPlayer(Player& other, const Actions & unblockCard);
	void PlaceCard(Cardinals & piece, Board & board);
	//The player has the possibility of placing a card on the board
	//The player has to take a card from the deck(Unused cards)
	const void TakeCard(const size_t& index, UnusedCards& ue, std::string type);
	//This function shuffles cards from unused cards to generate players cards
	const void GeneratePlayerCards(UnusedCards& other);
	//We'll add another random card from unused cards.
public:
	//getters
	const std::string& getName() const;
	const Actions& getAmIBlocked() const;
	const PlayerType& getWhatAmI() const;

	//setters
	void setName(const std::string& other);
	void setPlayerType(const PlayerType& other);

	const bool& operator==(const Player& other)const;

public:
	std::string name;
	Actions amIBlocked;
	PlayerType whatAmI;
	//The game rules say that players from 3-5 get 6 cards each
	const static size_t numOfPlayerCards = 6;
	//This array represents the cards the player has
	//We will move cards from unused cards to this array
	std::vector<Actions> myCards;

	//Remembers player's actions cards 
	std::vector<Cardinals> rememberRoadCards;
};

