#pragma once

#include <array>
#include <optional>
#include <iostream>

#include "Cards.h"
#include "Cardinale.h"

class Road :
	public Cards
{
	//5 carti de tip + care nu se vor roti
	//5 carti de tip T - vertical care se pot roti la 180 stg sau dr
	//5 carti de tip T - orizontal care se pot roti la 180 stg sau dr
	//4 carti tip L - oglinda --||--
	//3 tip - nerotibil
	//4 tip | nerotibil
	//5 tip L normal rotibil
	//31 road types

	//enum class plusType ->contine plusul normal , plusul deadend si startul

	//O rotire reprezinta interschimbarea valorii nor cu sud si est cu vest

	//Stim ca fiecare carte va fi semi-reprezentata ca o matrice
	// - - - -
	// N E S V
	// 5 carti cu echivalentul 1111 (cruce)
	// 5 carti cu echivalentul 0111 sau la rotire 1101 (T - vertical)
	// 5 carti cu echivalentul 1110 sau la rotire 1011 (T - orizontal)
	// 5 carti cu echivalentul 1100 sau la rotire 0011 (L - normal)
	// 5 carti cu echivalentul 1001 sau la rotire 0110 (L - oglindit)
	// 5 carti cu echivalentul 1010 (linie verticala)
	// 5 carti cu echivalentul 0101 (linie orizontala)
	// Total 35 de tipuri de carti cu drumuri continuabile
	// Total: 20 carti ce beneficiaza de posibilitatea de rotire

public:
	// Constants that represents the road cards
	Cardinals plusType;      //5 of this type
	Cardinals verticalT;     //5 of this type
	Cardinals horizontalT;   //5 of this type
	Cardinals normalL;       //5 of this type
	Cardinals mirroredL;     //4 of this type
	Cardinals verticalLine;  //4 of this type
	Cardinals horizontalLine;//3 of this type

	// Constants that represents the dead end cards, for the dead end cards we need just one from each type
	Cardinals plusTypeDeadEnd;
	Cardinals verticalTDeadEnd;
	Cardinals horizontalTDeadEnd;
	Cardinals normalLDeadEnd;
	Cardinals mirroredLDeadEnd;
	Cardinals verticalLineDeadEnd;
	Cardinals horizontalLineDeadEnd;
	Cardinals hookDeadEnd;
	Cardinals oneWayDeadEnd;

	static const size_t numberOfRoadsCard = 35;
	static const size_t numberOfRoadCard = 5;

public:
	//Constructors
	Road();

	//Destructor
	~Road() = default;

	Road& operator=(const Road& other);
	Road& operator=(Road&&other);

	//Getters
public:
	const Cardinals& getPlusType();
	const Cardinals& getVerticalT();
	const Cardinals& getHorizontalT();
	const Cardinals& getNormalL();
	const Cardinals& getMirroredL();
	const Cardinals& getVerticalLine();
	const Cardinals& getHorizontalLine();

	const Cardinals& getPlusTypeDeadEnd();
	const Cardinals& getVerticalTDeadEnd();
	const Cardinals& getHorizontalTDeadEnd();
	const Cardinals& getNormalLDeadEnd();
	const Cardinals& getMirroredLDeadEnd();
	const Cardinals& getVerticalLineDeadEnd();
	const Cardinals& getHorizontalLineDeadEnd();
	const Cardinals& getHookDeadEnd();
	const Cardinals& getOneWayDeadEnd();

public:
	//Generate all of the road cards to be as I said at the top of this class
	const void generatePlusType();
	const void generateVerticalT();
	const void generateHorizontalT();
	const void generateNormalL();
	const void generateMirroredL();
	const void generateVerticalLine();
	const void generateHorizontalLine();

	//Generate all of the dead end roads, because we need to know the diffrence betwen BLOCKED road (non-existent) and DEADEND road (not-possible)
	const void generatePlusTypeDeadEnd();
	const void generateVerticalTDeadEnd();
	const void generateHorizontalTDeadEnd();
	const void generateNormalLDeadEnd();
	const void generateMirroredLDeadEnd();
	const void generateVerticalLineDeadEnd();
	const void generateHorizontalLineDeadEnd();
	const void generateHookDeadEnd();
	const void generateOneWayDeadEnd();
};

