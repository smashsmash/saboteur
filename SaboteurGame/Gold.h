#pragma once
#include <array>

class Gold
{
public:
	enum class GoldEn : uint8_t
	{
		None,
		One,
		Two,
		Three
	};

public:
	//getter
	const GoldEn& getGold() const;
	//setter
	void setGold(const GoldEn& other);
	//operators
	Gold& operator = (const Gold& other);
	Gold& operator = (Gold&& other);
	friend std::ostream& operator<<(std::ostream& os, const Gold& other);


public:
	//4 cards of 3 gold pieces
	//8 cards of 2 gold pieces
	//16 cards of 1 gold piece

//constructors
	Gold();
	Gold(const Gold& other);
	Gold(const GoldEn& gold);
//destructor
	~Gold() = default;
	
public:
	const static size_t kNumberOfGoldCards = 28;
	GoldEn m_gold : 2;   // :2 -> max 4 componenents
};

