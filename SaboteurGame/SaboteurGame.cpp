#include "SaboteurGame.h"
#include "Board.h"
#include "UnusedCards.h"
#include "Tutorial.h"
#include "Gold.h"
#include "Player.h"
#include "PlayerType.h"

#include <string>
#include <ctime>
#include <algorithm>

Player& getPlayer(const std::string& name, std::vector<Player> player)
{
	for (auto idx = 0; idx < player.size(); idx++)
	{
		if (player[idx].getName() == name)
			return player[idx];
	}
}

void SaboteurGame::showRoadCards(const Player & pl)
{
	for (auto ind = 0; ind < pl.rememberRoadCards.size(); ++ind) {
		if (
			pl.rememberRoadCards[ind].getEast() != Cardinals::East::None &&
			pl.rememberRoadCards[ind].getNorth() != Cardinals::North::None &&
			pl.rememberRoadCards[ind].getSouth() != Cardinals::South::None &&
			pl.rememberRoadCards[ind].getWest() != Cardinals::West::None
			)
			std::cout << pl.rememberRoadCards[ind] << " ";
	}
}

void SaboteurGame::showActionCards(const Player & pl)
{
	for (auto ind = 0; ind < pl.myCards.size(); ++ind)
		if (pl.myCards[ind].getSpecialCard() != Actions::specialCards::None ||
			pl.myCards[ind].getToolOne() != Actions::tool::None ||
			pl.myCards[ind].getToolTwo() != Actions::tool::None ||
			pl.myCards[ind].getType() != Actions::type::None)
			std::cout << pl.myCards[ind] << " ";
}

void SaboteurGame::whatCardIsInPrize(const Board& b, const Board::position & poz)
{
	std::cout << "\nThe prize you unlocked is ";
	Road road;
	Cardinals check = road.getPlusType();
	if (b[poz] == check) {
		std::cout << "GOLD.\n";
	}
	else {
		std::cout << "STONE.\n";
	}
}

void SaboteurGame::generateNumbOfSBandGD(const size_t & op, size_t & SB, size_t & GD)
{

	//3 players: 1 saboteur  and 3 gold miners
	if (op == 3) {
		SB = 1;
		GD = 3;
	}
	//4 players: 1 saboteur  and 4 gold miners
	if (op == 4) {
		SB = 1;
		GD = 4;
	}
	//5 players: 2 saboteurs and 4 gold miners
	if (op == 5) {
		SB = 2;
		GD = 4;
	}
	//6 players: 2 saboteurs and 5 gold miners
	if (op == 6) {
		SB = 2;
		GD = 5;
	}
	//7 players: 3 saboteurs and 5 gold miners
	if (op == 7) {
		SB = 3;
		GD = 5;
	}
	//8 players: 3 saboteurs and 6 gold miners
	if (op == 8) {
		SB = 3;
		GD = 6;
	}
	//9 players: 3 saboteurs and 7 gold miners
	if (op == 9) {
		SB = 3;
		GD = 7;
	}
	//10 players: 4 saboteurs and 7 gold miners
	if (op == 10) {
		SB = 4;
		GD = 7;
	}
}

void SaboteurGame::randomizePlayers(Player & player, size_t& indexGD, size_t& indexSB, size_t& maxOfSb, size_t& maxOfGD)
{
	PlayerType pt;
	size_t randomize = rand() % 2;
	if (randomize == 0) {
		if (indexSB == maxOfSb)
			randomize = 1;
		else {
			pt.m_playerType = PlayerType::PlayerTypeEn::Saboteur;
			player.setPlayerType(pt);
			indexSB++;
		}
	}
	if (randomize == 1) {
		if (indexGD == maxOfGD)
			randomize = 0;
		else {
			pt.m_playerType = PlayerType::PlayerTypeEn::Midget;
			player.setPlayerType(pt);
			indexGD++;
		}
		if (randomize == 0) {
			pt.m_playerType = PlayerType::PlayerTypeEn::Saboteur;
			player.setPlayerType(pt);
			indexSB++;
		}
	}
}

void SaboteurGame::play()
{
	//Really BIG TO DO: Add comments to every 
	//function/method and lines from each header and cpp file including this one

	std::cout << "Do you know the game?(Y/N)";
	std::string answer;
	std::cin >> answer;
	if (answer != "Y"&&answer != "y")
	{
		Tutorial myTutorial;
		myTutorial.saboteurTutorial();
	}
	std::cout << "How many players will play?(3-10)";
	size_t nrOfPlayers;
	std::cin >> nrOfPlayers;
	while (nrOfPlayers < 3 || nrOfPlayers > 10) {
		std::cout << "The number is incorrect, try again\nHow many players will play?(3-10)";
		std::cin >> nrOfPlayers;
	}

	//defining all palyers
	std::vector<Player> players;
	players.resize(nrOfPlayers);
	players.shrink_to_fit();

	//defining if they're saboteurs or gold diggers
	size_t maxOfSb;
	size_t maxOfGD;

	generateNumbOfSBandGD(nrOfPlayers, maxOfSb, maxOfGD);
	size_t played = 0;
	Board myBoard;

	//The game sayes that the game has 3 rounds and the layer who has most gold wins
	while (played < 3) {
		UnusedCards stack;
		stack.generateUnusedCards();

		size_t indexSB = 0;
		size_t indexGD = 0;
		for (auto index = 0; index < players.size(); ++index) {
			randomizePlayers(players[index], indexGD, indexSB, maxOfSb, maxOfGD);
			std::cout << "Player no." << index << " enter your name:";
			std::cin >> players[index].name;
			std::cout << "Please pay atention!\n We will tell you what type of player you will be this game and then we will delete that line so the only one who know will be you!\n";
			if (players[index].getWhatAmI().getPlayerType() == PlayerType::PlayerTypeEn::Saboteur)
				std::cout << "SABOTEUR\n";
			else
				std::cout << "GOLD DIGGER\n";
			std::cout << "Good luck!\n";
			system("pause");
			system("cls");
		}

		//TO DO: add 2 new functions in Player to generate cards - road and action,and verify if they are correctly generated
		for (auto idx = 0; idx < players.size(); ++idx) {
			players[idx].GeneratePlayerCards(stack);
		}

		size_t indexToPlay;
		while (true) {
			std::cout << "The board:\n";
			std::cout << myBoard;
			std::cout << "The prizes are shuffeled\n";
			std::string s;
			for (auto index = 0; index < players.size(); ++index) {
				//We will display for the next ones the Board and each Player "position"
				if (index != 0) {
					std::cout << myBoard << std::endl;
					for (auto i = 0; i < players.size(); ++i)
					{
						std::cout << players[i].name << " is ";
						if (players[i].amIBlocked.m_type == Actions::type::Block)
							std::cout << "blocked :(\n";
						else
							std::cout << "playing :)\n";
					}
				}
				std::cout << players[index].name << "'s turn!\n";
				std::cout << "Your road cards: \n";
				showRoadCards(players[index]);
				std::cout << "\nYour action cards:\n";
				showActionCards(players[index]);
				std::cout << "\nWitch type of card would you like to use(R - for Road/A - for Actions/P - to pass)?";
				std::string opt;
				std::cin >> opt;
				while (opt != "A" && opt != "R" && opt != "a" && opt != "r" && opt!="p" && opt!="P") {
					std::cout << "The option is incorect!\n Try again..\n";
					std::cout << "\nWitch type of card would you like to use(Road/Action/Pass)?";
					std::cin >> opt;
				}

				if (opt == "r" || opt == "R")
				{
					showRoadCards(players[index]);
					std::cout << "\nWich card woul you like to use?(1-" << players[index].rememberRoadCards.size() << ")?\n";
					std::cin >> indexToPlay;
					s = "r";
					//The position
					Board::position placedPosition;
					while (true)
					{
						try
						{
							players[index].PlaceCard(players[index].rememberRoadCards[indexToPlay], myBoard);
						}
						catch (const char* errorMessage)
						{
							std::cout << errorMessage << std::endl;
						}
					}
				}
				else if(opt=="A"||opt=="a")
				{
					std::cout << "Wich card would you like to use?(1-" << players[index].myCards.size() << ")?\n";
					std::cin >> indexToPlay;
					//TO DO: we need to add in actions card a method that takes an Action card and does what the card should do
					if (players[index].myCards[indexToPlay].getSpecialCard() == Actions::specialCards::None)  //blocaj,deblocaj
					{
						s = "a";
						if (players[index].myCards[indexToPlay].getType() == Actions::type::Block)
						{
							std::cout << "What's the name of the victim? ";
							std::string name;
							std::cin >> name;
							auto ind = std::find(players.begin(), players.end(), getPlayer(name, players));
							
							ind->amIBlocked = players[index].myCards[indexToPlay];
						}
						else{
								std::cout << "Who  do you wanna rescue? ";
								std::string name;
								std::cin >> name;
								Player p = getPlayer(name, players);
								auto ind = std::find(players.begin(), players.end(), p);
								
								try {
									players[index].UnblockPlayer(*ind, players[index].myCards[indexToPlay]);
								}
								catch (char* errMsg) {
									std::cout << errMsg;
								}
						}
					}
					else
						if (players[index].myCards[indexToPlay].getSpecialCard() == Actions::specialCards::Map)
						{
							s = "a";
							std::cout << "So..you want to find out a Prize..Which one shoul it be? 1,2 or 3?\n";
							size_t prize;
							std::cin >> prize;

							if (prize == 1)
								whatCardIsInPrize(myBoard, { 0,7 });
							else
								if (prize == 2)
									whatCardIsInPrize(myBoard, { 2,7 });
								else
									whatCardIsInPrize(myBoard, { 4,7 });
						}
						else
						{
							std::cout << "Enter position of the card you want to remove..\n";
							Board::position poz;
							std::cin >> poz.first;
							std::cin >> poz.second;
							myBoard.removeCard(poz);
							s = "a";
						}
					//Ask it he will pass this round
					//TakeCard
					//Check if the game ended this round

					//Go up and see why the Board doesn't print the pieces but everything else does it right
				}
				else {
					std::cout << "Index to the card that will be deleted\n";
					std::cin >> indexToPlay;
					s = "r";
				}
				players[index].TakeCard(indexToPlay, stack, s);
				//We will make disapear all the choices and cards that the player has made
				std::cout << "Everything you've choosen will be hidden from the other players.\n";
				system("pause");
				system("cls");
			}
		}
	}

}
