﻿#include <iostream>
#include "Cardinale.h"
#include "UnusedCards.h"
#include "Tutorial.h"
#include "Board.h"
#include "SaboteurGame.h"

#include"../Logging/Logging.h"
#include<fstream>

void tests() {
	//A little test to see the output for our cards
	Cardinals y(Cardinals::North::DeadEnd, Cardinals::South::DeadEnd, Cardinals::East::DeadEnd, Cardinals::West::DeadEnd);
	Cardinals z(Cardinals::North::Blocked, Cardinals::South::Blocked, Cardinals::East::Blocked, Cardinals::West::Blocked);
	std::cout << " " << y << " " << z << " ";
	std::cout << std::endl;

	//Output of board
	Board d;
	std::cout << d;

	//Output of unusedCards
	UnusedCards x;
	x.generateUnusedCards();
	std::cout << x;

	//Output of tutorial
	Tutorial tut;
	tut.saboteurTutorial();

}
/*



Each player will recieve a number of cards, the cards will be generated randomly,
combined (RoadCards and Actions) depending on the number of players in the game:

3 to 5 players: Each player is dealt 6 cards.
6 to 7 players: Each player is dealt 5 cards.
8 to 10 players: Each player is dealt 4 cards.
*/

int main() {
	/*std::ofstream of("syslog.log", std::ios::app);
	Logger logger(of);

	logger.log("Started Application...", Logger::Level::Info);*/
	system("pause");
	SaboteurGame sabGame;
	sabGame.play();
	system("pause");
	return 0;
}