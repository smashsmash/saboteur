#include "Road.h"

//Road::Road(const Road & other):
//	allRoadTypes(other.allRoadTypes)
//{
//	//Copy constructor
//}

Road::Road()
{
	generatePlusType();
	generateVerticalT();
	generateHorizontalT();
	generateNormalL();
	generateMirroredL();
	generateVerticalLine();
	generateHorizontalLine();

	generatePlusTypeDeadEnd();
	generateVerticalTDeadEnd();
	generateHorizontalTDeadEnd();
	generateNormalLDeadEnd();
	generateMirroredLDeadEnd();
	generateVerticalLineDeadEnd();
	generateHorizontalLineDeadEnd();
	generateHookDeadEnd();
	generateOneWayDeadEnd();
}

Road & Road::operator=(const Road & other)
{
	plusType = other.plusType;
	verticalT = other.verticalT;
	horizontalT = other.horizontalT;
	normalL = other.normalL;
	mirroredL = other.mirroredL;
	verticalLine = other.verticalLine;
	horizontalLine = other.horizontalLine;

	plusTypeDeadEnd = other.plusTypeDeadEnd;
	verticalTDeadEnd = other.verticalLineDeadEnd;
	horizontalTDeadEnd = other.horizontalTDeadEnd;
	normalLDeadEnd = other.normalLDeadEnd;
	mirroredLDeadEnd = other.mirroredLDeadEnd;
	verticalLineDeadEnd = other.verticalLineDeadEnd;
	horizontalLineDeadEnd = other.horizontalLineDeadEnd;
	hookDeadEnd = other.hookDeadEnd;
	oneWayDeadEnd = other.oneWayDeadEnd;

	return *this;
}

Road & Road::operator=(Road && other)
{

	plusType = other.plusType;
	verticalT = other.verticalT;
	horizontalT = other.horizontalT;
	normalL = other.normalL;
	mirroredL = other.mirroredL;
	verticalLine = other.verticalLine;
	horizontalLine = other.horizontalLine;

	plusTypeDeadEnd = other.plusTypeDeadEnd;
	verticalTDeadEnd = other.verticalLineDeadEnd;
	horizontalTDeadEnd = other.horizontalTDeadEnd;
	normalLDeadEnd = other.normalLDeadEnd;
	mirroredLDeadEnd = other.mirroredLDeadEnd;
	verticalLineDeadEnd = other.verticalLineDeadEnd;
	horizontalLineDeadEnd = other.horizontalLineDeadEnd;
	hookDeadEnd = other.hookDeadEnd;
	oneWayDeadEnd = other.oneWayDeadEnd;

	new(&other) Road;

	return *this;
}

const void Road::generatePlusType()
{
	plusType.m_East = Cardinals::East::Available;
	plusType.m_North = Cardinals::North::Available;
	plusType.m_South = Cardinals::South::Available;
	plusType.m_West = Cardinals::West::Available;

}

const void Road::generateVerticalT()
{
	verticalT.m_East = Cardinals::East::Available;
	verticalT.m_North = Cardinals::North::Blocked;
	verticalT.m_South = Cardinals::South::Available;
	verticalT.m_West = Cardinals::West::Available;
}

const void Road::generateHorizontalT()
{
	horizontalT.m_East = Cardinals::East::Available;
	horizontalT.m_North = Cardinals::North::Available;
	horizontalT.m_South = Cardinals::South::Available;
	horizontalT.m_West = Cardinals::West::Blocked;
}

const void Road::generateNormalL()
{
	normalL.m_East = Cardinals::East::Available;
	normalL.m_North = Cardinals::North::Available;
	normalL.m_South = Cardinals::South::Blocked;
	normalL.m_West = Cardinals::West::Blocked;
}

const void Road::generateMirroredL()
{
	mirroredL.m_North = Cardinals::North::Available;
	mirroredL.m_South = Cardinals::South::Blocked;
	mirroredL.m_West = Cardinals::West::Available;
	mirroredL.m_East = Cardinals::East::Blocked;
}

const void Road::generateVerticalLine()
{
	verticalLine.m_East = Cardinals::East::Blocked;
	verticalLine.m_North = Cardinals::North::Available;
	verticalLine.m_South = Cardinals::South::Available;
	verticalLine.m_West = Cardinals::West::Blocked;
}

const void Road::generateHorizontalLine()
{
	horizontalLine.m_East = Cardinals::East::Available;
	horizontalLine.m_West = Cardinals::West::Available;
	horizontalLine.m_North = Cardinals::North::Blocked;
	horizontalLine.m_South = Cardinals::South::Blocked;
}

const void Road::generatePlusTypeDeadEnd()
{
	plusTypeDeadEnd.m_East = Cardinals::East::DeadEnd;
	plusTypeDeadEnd.m_West = Cardinals::West::DeadEnd;
	plusTypeDeadEnd.m_North = Cardinals::North::DeadEnd;
	plusTypeDeadEnd.m_South = Cardinals::South::DeadEnd;
}

const void Road::generateVerticalTDeadEnd()
{
	verticalTDeadEnd.m_East = Cardinals::East::DeadEnd;
	verticalTDeadEnd.m_North = Cardinals::North::Blocked;
	verticalTDeadEnd.m_South = Cardinals::South::DeadEnd;
	verticalTDeadEnd.m_West = Cardinals::West::DeadEnd;
}

const void Road::generateHorizontalTDeadEnd()
{
	horizontalTDeadEnd.m_East = Cardinals::East::DeadEnd;
	horizontalTDeadEnd.m_North = Cardinals::North::DeadEnd;
	horizontalTDeadEnd.m_South = Cardinals::South::DeadEnd;
	horizontalTDeadEnd.m_West = Cardinals::West::Blocked;
}

const void Road::generateNormalLDeadEnd()
{
	normalLDeadEnd.m_East = Cardinals::East::DeadEnd;
	normalLDeadEnd.m_North = Cardinals::North::DeadEnd;
	normalLDeadEnd.m_South = Cardinals::South::Blocked;
	normalLDeadEnd.m_West = Cardinals::West::Blocked;
}

const void Road::generateMirroredLDeadEnd()
{
	mirroredLDeadEnd.m_East = Cardinals::East::Blocked;
	mirroredLDeadEnd.m_North = Cardinals::North::DeadEnd;
	mirroredLDeadEnd.m_South = Cardinals::South::Blocked;
	mirroredLDeadEnd.m_West = Cardinals::West::DeadEnd;
}

const void Road::generateVerticalLineDeadEnd()
{
	verticalLineDeadEnd.m_East = Cardinals::East::Blocked;
	verticalLineDeadEnd.m_North = Cardinals::North::DeadEnd;
	verticalLineDeadEnd.m_South = Cardinals::South::DeadEnd;
	verticalLineDeadEnd.m_West = Cardinals::West::Blocked;
}

const void Road::generateHorizontalLineDeadEnd()
{
	horizontalLineDeadEnd.m_East = Cardinals::East::DeadEnd;
	horizontalLineDeadEnd.m_North = Cardinals::North::Blocked;
	horizontalLineDeadEnd.m_South = Cardinals::South::Blocked;
	horizontalLineDeadEnd.m_West = Cardinals::West::DeadEnd;
}

const void Road::generateHookDeadEnd()
{
	hookDeadEnd.m_East = Cardinals::East::DeadEnd;
	hookDeadEnd.m_North = Cardinals::North::Blocked;
	hookDeadEnd.m_South = Cardinals::South::Blocked;
	hookDeadEnd.m_West = Cardinals::West::Blocked;
}

const void Road::generateOneWayDeadEnd()
{
	oneWayDeadEnd.m_East = Cardinals::East::Blocked;
	oneWayDeadEnd.m_North = Cardinals::North::DeadEnd;
	oneWayDeadEnd.m_South = Cardinals::South::Blocked;
	oneWayDeadEnd.m_West = Cardinals::West::Blocked;
}

const Cardinals & Road::getPlusType()
{
	return plusType;
}

const Cardinals & Road::getVerticalT()
{
	return verticalT;
}

const Cardinals & Road::getHorizontalT()
{
	return horizontalT;
}

const Cardinals & Road::getNormalL()
{
	return normalL;
}

const Cardinals & Road::getMirroredL()
{
	return mirroredL;
}

const Cardinals & Road::getVerticalLine()
{
	return verticalLine;
}

const Cardinals & Road::getHorizontalLine()
{
	return horizontalLine;
}

const Cardinals & Road::getPlusTypeDeadEnd()
{
	return plusTypeDeadEnd;
}

const Cardinals & Road::getVerticalTDeadEnd()
{
	return verticalTDeadEnd;
}

const Cardinals & Road::getHorizontalTDeadEnd()
{
	return horizontalTDeadEnd;
}

const Cardinals & Road::getNormalLDeadEnd()
{
	return normalLDeadEnd;
}

const Cardinals & Road::getMirroredLDeadEnd()
{
	return mirroredLDeadEnd;
}

const Cardinals & Road::getVerticalLineDeadEnd()
{
	return verticalLineDeadEnd;
}

const Cardinals & Road::getHorizontalLineDeadEnd()
{
	return horizontalLineDeadEnd;
}

const Cardinals & Road::getHookDeadEnd()
{
	return hookDeadEnd;
}

const Cardinals & Road::getOneWayDeadEnd()
{
	return oneWayDeadEnd;
}
