#pragma once
#include "Player.h"
class SaboteurGame
{
public:
	void showRoadCards(const Player& pl);
	void showActionCards(const Player& pl);
	void whatCardIsInPrize(const Board& b, const Board::position& poz);
	void generateNumbOfSBandGD(const size_t& op, size_t& SB, size_t& GD);
	void randomizePlayers(Player & player, size_t & indexGD, size_t & indexSB, size_t & maxOfSb, size_t & maxOfGD);

	void play();
};

